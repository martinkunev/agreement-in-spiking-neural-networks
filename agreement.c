/*
 * Agreement in SNN
 * Copyright (C) 2022  Martin Kunev <martinkunev@gmail.com>
 *
 * This file is part of Agreement in SNN.
 *
 * Agreement in SNN is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation version 3 of the License.
 *
 * Agreement in SNN is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Agreement in SNN.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DELTA 0.33
#define SIGMOID_FACTOR 1

#define VERTEX_INPUT 0
#define VERTEX_OUTPUT 1

#define ROUNDS 32
#define TESTS_FIXED 256

#define firing(pattern, vertex) ((pattern) & (1 << (vertex)))

struct snn
{
	struct
	{
		size_t count;
		struct vertex
		{
			double potential_external;
			double potential;
			short bias;
		} vertex[];
	} *L;

	struct
	{
		size_t count;
		struct edge
		{
			enum type {INTERNAL, EXTERNAL} type; // TODO add 2 more types for M
			unsigned char in, out;
			short weight;
		} edge[];
	} *edges;

	struct
	{
		unsigned current;
		unsigned next;
	} *firing; // number of components with the given pattern on current and next round
	size_t firepatterns_count;

	double W;
	unsigned n;
	unsigned inputs, outputs;
	unsigned round;
};

static inline double sigmoid(double x)
{
	return 1 / (1 + exp(- SIGMOID_FACTOR * x));
}

static inline int activate(double potential)
{
	return sigmoid(potential) >= ((double)random() / RAND_MAX);
}

static void snn_init(struct snn *restrict network, size_t component_vertices_count, size_t edges_count, size_t n, double W)
{
	size_t i;

	// at least 1 input and output vertex
	assert(component_vertices_count >= 2);

	// vertex index should fit in a byte and leave 1 bit free
	//assert(component_vertices_count <= (256 >> 1));

	// ensure the number of subsets of vertices is not too big
	assert(component_vertices_count <= 15);

	network->L = malloc(sizeof(*network->L) + component_vertices_count * sizeof(*network->L->vertex));
	if (!network->L)
		abort();

	network->edges = malloc(sizeof(*network->edges) + edges_count * sizeof(*network->edges->edge));
	if (!network->edges)
		abort();

	network->firepatterns_count = 1 << component_vertices_count;
	network->firing = malloc(sizeof(*network->firing) * network->firepatterns_count);
	if (!network->firing)
		abort();

	network->L->count = component_vertices_count;

	network->edges->count = edges_count;

	network->W = W;
	network->n = n;
}

static inline void snn_set_bias(struct snn *restrict network, size_t index, short bias)
{
	// Input neurons have no biases.
	assert(index != 0);

	network->L->vertex[index].bias = bias;
}

static inline void snn_set_edge(struct snn *restrict network, size_t index, unsigned char in, unsigned char out, short weight, enum type type)
{
	network->edges->edge[index] = (struct edge){.type = type, .in = in, .out = out, .weight = weight};
}

static void snn_start(struct snn *restrict network, unsigned inputs)
{
	size_t i;

	network->inputs = inputs;
	network->outputs = 0;
	network->round = 0;

	network->firing[0].current = network->n - inputs;
	network->firing[1].current = inputs;
	for(i = 2; i < network->firepatterns_count; i += 1)
		network->firing[i].current = 0;
}

static unsigned snn_step(struct snn *network)
{
	size_t i, j, k;

	// Potential coming from other subnetworks is the same for all vertices.
	// Calculate this potential first.
	for(i = 0; i < network->L->count; i += 1)
	{
		struct vertex *vertex = &network->L->vertex[i];
		vertex->potential_external = network->W * -vertex->bias;
	}
	for(i = 0; i < network->edges->count; i += 1)
	{
		struct edge *edge = &network->edges->edge[i];
		struct vertex *vertex;

		if (edge->type == INTERNAL)
			continue;
		vertex = &network->L->vertex[edge->out];

		for(j = 0; j < network->firepatterns_count; j += 1)
		{
			if (!firing(j, edge->in))
				continue; // the in vertex is not firing in this subset

			vertex->potential_external += network->W * edge->weight * network->firing[j].current;
		}
	}

	for(i = 0; i < network->firepatterns_count; i += 1)
		network->firing[i].next = 0;

	network->outputs = 0;

	// Calculate the exact potential for each subnetwork.
	// Determine which neurons fire.
	for(i = 0; i < network->firepatterns_count; i += 1)
	{
		if (!network->firing[i].current)
			continue; // no subnetworks with this firing pattern

		for(j = 0; j < network->L->count; j += 1)
		{
			struct vertex *vertex = &network->L->vertex[j];
			vertex->potential = vertex->potential_external;
		}

		for(j = 0; j < network->edges->count; j += 1)
		{
			struct edge *edge = &network->edges->edge[j];
			struct vertex *vertex;

			if (edge->type == EXTERNAL)
				continue;

			if (!firing(i, edge->in))
				continue; // the in vertex is not firing in this subset

			vertex = &network->L->vertex[edge->out];
			vertex->potential += network->W * edge->weight;
		}

		for(k = 0; k < network->firing[i].current; k += 1)
		{
			size_t index = firing(i, VERTEX_INPUT);

			for(j = 1; j < network->L->count; j += 1)
			{
				struct vertex *vertex = &network->L->vertex[j];

				if (activate(vertex->potential))
					index |= 1 << j;
			}

			if (firing(index, VERTEX_OUTPUT))
				network->outputs += 1;
			network->firing[index].next += 1;
		}
	}

	for(i = 0; i < network->firepatterns_count; i += 1)
		network->firing[i].current = network->firing[i].next;

	network->round += 1;
	return network->outputs;
}

static void snn_term(struct snn *network)
{
	free(network->firing);
	free(network->edges);
	free(network->L);
}

static void network_agreement(struct snn *network, size_t n)
{
	unsigned edge = 0, edges = 5;

	snn_init(network, 3, edges, n, 2 * log((1 - DELTA) / DELTA));

	snn_set_bias(network, 2, -1);
	snn_set_bias(network, VERTEX_OUTPUT, 0);

	snn_set_edge(network, edge++, VERTEX_INPUT, VERTEX_OUTPUT, 2, INTERNAL);
	snn_set_edge(network, edge++, VERTEX_INPUT, 2, -2, INTERNAL);
	snn_set_edge(network, edge++, 2, VERTEX_OUTPUT, -2, INTERNAL);
	snn_set_edge(network, edge++, 2, VERTEX_OUTPUT, -2, EXTERNAL);
	snn_set_edge(network, edge++, VERTEX_OUTPUT, VERTEX_OUTPUT, 2, EXTERNAL);

	assert(edge == edges);
}

static void test(struct snn *restrict network, unsigned inputs, unsigned *restrict count)
{
	snn_start(network, inputs);

	while (network->round < ROUNDS)
	{
		size_t round = network->round; // snn_step() changes round
		count[round] = snn_step(network);
	}
}

static void store(unsigned input, const unsigned *count)
{
	size_t i;
	printf("%u", input);
	for(i = 0; i < ROUNDS; i += 1)
		printf(",%u", count[i]);
	putchar('\n');
}

int main(int argc, char *argv[])
{
	struct snn network;
	size_t i;
	unsigned n;
	unsigned count[ROUNDS];

	srandom(time(0));

	if (argc != 2)
	{
		fprintf(stderr, "usage: %s <n>\n", argv[0]);
		return 1;
	}
	n = strtol(argv[1], 0, 10);
	if (n < 8)
	{
		fprintf(stderr, "n must be at least 8\n");
		return 2;
	}
	printf("# %u\n", n);

	network_agreement(&network, n);

	for(i = 0; i < TESTS_FIXED; i += 1)
	{
		test(&network, 0, count);
		store(network.inputs, count);

		test(&network, network.n, count);
		store(network.inputs, count);

		test(&network, network.n / 2, count);
		store(network.inputs, count);
	}

	//for(i = 0; i < TESTS_FIXED; i += 1)
	for(i = 0; i < network.n * 2; i += 1)
	{
		test(&network, random() % (network.n + 1), count);
		store(network.inputs, count);
	}

	snn_term(&network);

	return 0;
}
