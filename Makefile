CC=gcc
CFLAGS=-g -O0
#CFLAGS=-O2 -fomit-frame-pointer -DNDEBUG
LDFLAGS=-g -lm
#LDFLAGS=-O2 -lm

all: agreement

agreement: agreement.o
#	$(CC) $(CFLAGS) $^ -o $@

test:
	true
	# for i in 8 9 10 11 12 13 14 15 16 32 64 128 256 512 1024; do sed -i -e 's/#define WIDTH .*/#define WIDTH '$i'/g' agreement.c && make clean all && ./agreement > "results${i}_01"; done

clean:
	rm -f *.o
	rm -f agreement
