#!/usr/bin/python3

"""
Agreement in SNN
Copyright (C) 2022  Martin Kunev <martinkunev@gmail.com>

This file is part of Agreement in SNN.

Agreement in SNN is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation version 3 of the License.

Agreement in SNN is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Agreement in SNN.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import math
import csv
import matplotlib.pyplot as plt

plt.xlabel("log n")
#plt.xlim(0, 70)
#plt.xlim(0, 500000)
plt.ylabel("time")
plt.ylim(0, +8)
#plt.show()

#plt.rcParams.update({"font.size": 9})
#plt.rc("axes", titlesize=6)
#plt.rc("axes", labelsize=6)
#plt.rc("xtick", labelsize=6)
#plt.rc("ytick", labelsize=6)

x = []
y = []

agreements = {}
mean = {}
moment2 = {}

for arg in sys.argv[1:]:
	f = open(arg)
	n = int(f.readline()[1:])
	reader = csv.reader(f)

	agreements[n] = []
	rounds_total = 0
	squares = 0

	count = 0
	for row in reader:
		input = int(row[0])
		outputs = [1] + list(map(int, row[1:]))

		all0, all1 = [], []

		if input < n:
			all0 = [o[0] for o in enumerate(outputs) if o[1] == 0]
		if not len(all0):
			all0 = [len(outputs)]

		if input > 0:
			all1 = [o[0] for o in enumerate(outputs) if o[1] == n]
		if not len(all1):
			all1 = [len(outputs)]

		count += 1

		rounds_agreement = min(all0[0], all1[0])
		if rounds_agreement == len(outputs):
			print("no convergence for {}".format(arg))
			continue

		if rounds_agreement >= 20:
			print(input, n, rounds_agreement, count, all0[:20])

		agreements[n] += [rounds_agreement]
		rounds_total += rounds_agreement
		squares += rounds_agreement * rounds_agreement
	f.close()

	mean[n] = rounds_total / count
	moment2[n] = squares / count

for n in sorted(mean.keys()):
	print("{}	{}		{}".format(n, mean[n], moment2[n] - mean[n] * mean[n]))

keys = sorted(mean.keys())

data = [agreements[n] for n in keys]
plt.boxplot(data)
plt.xticks(range(1, len(keys) + 1), [str(int(math.log2(key))) for key in keys])

x = [int(math.log2(n)) - 2 for n in keys]
y = [mean[n] for n in keys]
plt.plot(x, y, color="green")

plt.savefig("plot.png", dpi=300)

